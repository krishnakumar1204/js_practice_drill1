//6    Create a function to retrieve and display the first hobby of each individual in the dataset.

function printFirstHobby(arrayOfObjects){
    if(Array.isArray(arrayOfObjects)){
        for(person of arrayOfObjects){
            console.log(person.hobbies[0]);
        }
    }
    else{
        console.log([]);
    }
}

module.exports = printFirstHobby;