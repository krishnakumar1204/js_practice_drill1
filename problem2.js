//2    Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.


function printHobbies(arrayOfObjects, givenAge){
    if(Array.isArray(arrayOfObjects) && typeof givenAge == 'number'){
        for(person of arrayOfObjects){
            if(person.age == givenAge){
                console.log(person.hobbies);
            }
        }
    }
    else{
        console.log(null);
    }
}

module.exports = printHobbies;