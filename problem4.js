//4    Write a function that accesses and logs the name and city of the individual at the index position 3 in the dataset.

function nameAndCityAtIndex3(arrayOfObjects){
    if(Array.isArray(arrayOfObjects)){
        let personAt3 = arrayOfObjects[3];
        console.log(personAt3.name + " from " + personAt3.city);
    }
    else{
        console.log([]);
    }
}

module.exports = nameAndCityAtIndex3;