//1    Given the dataset of individuals, write a function that accesses and returns the email addresses of all individuals.


function findEmail(arrayOfObjects){
    let emailAddress = [];
    if(Array.isArray(arrayOfObjects)){
        for(person of arrayOfObjects){
            emailAddress.push(person.email);
        }
        return emailAddress;
    }
    else{
        return [];
    }
}

module.exports = findEmail;