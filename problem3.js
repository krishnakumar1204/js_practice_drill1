//3    Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.


function displayName(arrayOfObjects){
    if(Array.isArray(arrayOfObjects)){
        for(person of arrayOfObjects){
            if(person.isStudent && person.country=="Australia"){
                console.log(person.name);
            }
        }
    }
    else{
        console.log(null);
    }
}

module.exports = displayName;