//8    Implement a loop to access and log the city and country of each individual in the dataset.

function printCityAndCountry(arrayOfObjects){
    if(Array.isArray(arrayOfObjects)){
        for(person of arrayOfObjects){
            console.log(person.city + ", " + person.country);
        }
    }
    else{
        console.log([]);
    }
}

module.exports = printCityAndCountry;