//7    Write a function that accesses and prints the names and email addresses of individuals aged 25.


function printNameAndEmailId(arrayOfObjects, num){
    if(Array.isArray(arrayOfObjects)){
        for(person of arrayOfObjects){
            if(person.age == num){
                console.log(person.name + " " + person.email);
            }
        }
    }
    else{
        console.log([]);
    }
}

module.exports = printNameAndEmailId;