//5    Implement a loop to access and print the ages of all individuals in the dataset.

function printAge(arrayOfObjects){
    if(Array.isArray(arrayOfObjects)){
        for(person of arrayOfObjects){
            console.log(person.age);
        }
    }
    else{
        console.log([]);
    }
}

module.exports = printAge;